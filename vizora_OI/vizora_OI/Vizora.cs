﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vizora_OI
{
    class Vizora
    {
        string nev;

        public string Nev
        {
            get { return nev; }
            set { nev = value; }
        }
        double allas=0.0;

        public double Allas
        {
            get 
            { return allas; }
            set 
            {
                if (value > 0)
                {
                    allas = value;
                }
                else
                {
                    Console.WriteLine("Az óraállás csak pozitív érték lehet!");
                }
             }
        }
        DateTime idopont;

        public DateTime Idopont
        {
            get { return idopont; }
            set { idopont = value; }
        }
        List<double> korabbiAllasok = new List<double>();

        public List<double> KorabbiAllasok
        {
            get { return korabbiAllasok; }
            set { korabbiAllasok = value; }
        }
    }
}
