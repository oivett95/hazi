﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vizora_OI
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Vizora> vizorak = new List<Vizora>();
            Console.WriteLine("Menü:\n- új vízóra felvétele: új óra\n- óraállás megadása: óraállás\n- vízóra lekérdezése: lekérdezés\n- kilépés: EXIT");
            string menupont = Console.ReadLine();
            while (menupont != "EXIT")
            {
                if (menupont == "új óra")
                {
                    Vizora vizora = new Vizora();
                    Console.Write("Adja meg a vízóra nevét: ");
                    vizora.Nev = Console.ReadLine();
                    vizorak.Add(vizora);
                    menupont = "";
                }
                else if (menupont == "óraállás")
                {
                    oraallas(vizorak);
                }
                else if (menupont == "lekérdezés")
                {
                    Console.WriteLine(lekerdezes(vizorak));
                }
                else
                {
                    Console.WriteLine("Nem értelmezhető parancs!");
                }
   

                menupont = Console.ReadLine();
            }

            
        }

        
        private static string lekerdezes(List<Vizora> vizorak)
        {
            StringBuilder sb=new StringBuilder();
            bool b = false;
            if (vizorak.Count==0)
            {
                sb.AppendFormat("Még nem adott meg vízórát!");
            }
            else 
            {
                Console.Write("Adja meg az óra nevét: ");
                string nev = Console.ReadLine();
                for (int i = 0; i < vizorak.Count; i++)
                {
                    if (vizorak[i].Nev == nev)
                    {
                        b = true;
                        if (vizorak[i].Allas==0.0)
                        {
                            sb.AppendFormat("Ehhez az órához még nem adott meg állást!");

                        }
                        else
                        {
                            Console.WriteLine();
                            sb.AppendFormat(vizorak[i].Nev+", "+vizorak[i].Allas.ToString()+" m3, "+vizorak[i].Idopont.ToString()+"\nKorábbi állások:\n");
                            for (int j = 0; j < vizorak[i].KorabbiAllasok.Count; j++)
                            {
                                sb.AppendFormat(vizorak[i].KorabbiAllasok[j].ToString() + " m3\n");   
                            }   
                        }
                    }
                    if (b == false)
                    {
                        sb.AppendFormat("Nincs ilyen nevű óra!");
                    }
                }
            }
            return sb.ToString();
        }

        private static void oraallas(List<Vizora> vizorak)
        {
            bool b = false;
            if (vizorak.Count == 0)
            {
                Console.WriteLine("Még nem adott meg vízórát!");
            }
            else
            {
                Console.Write("Adja meg az óra nevét: ");
                string nev = Console.ReadLine();
                for (int i = 0; i < vizorak.Count; i++)
                {
                    if (vizorak[i].Nev == nev)
                    {
                        b = true;
                        Console.Write("Adja meg az óraállást: ");
                        if (vizorak[i].Allas != 0.0)
                        {
                            vizorak[i].KorabbiAllasok.Add(vizorak[i].Allas);
                        }
                        string allas = Console.ReadLine();
                        double a;
                        if (double.TryParse(allas, out a))
                        {
                            vizorak[i].Allas = Convert.ToDouble(a);
                            vizorak[i].Idopont = DateTime.Now;
                        }
                        else
                        {
                            Console.WriteLine("Nem számot adott meg!");
                        }
                    }
                    if (b==false)
                    {
                        Console.WriteLine("Nincs ilyen nevű óra!");
                    }
                }

            }
        }



    }
}
