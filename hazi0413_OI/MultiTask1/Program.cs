﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiTask1
{
    class Program
    {
        static void Main(string[] args)
        {
            Restaurant r = new Restaurant();
            Task[] tasks = new Task[3];
            for (int i = 0; i < 3; i++)
            {
                tasks[i] = Task.Factory.StartNew(() => r.Guest(10));
            }
            Task t2 = Task.Factory.StartNew(() => r.Chef(10));
            try
            {
                Task.WaitAll(tasks);
            }
            catch (AggregateException aex)
            {
                foreach (Exception e in aex.InnerExceptions)
                {
                    Console.WriteLine(e.Message);
                }
            }
            Task.WaitAll(t2);
            Console.WriteLine("All finished!");

            Console.ReadLine();
        }
    }
}
