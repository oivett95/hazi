﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiTask1
{
    class Restaurant
    {
        private List<string> foods=new List<string>();
        public void Chef(int db)
        {
            for (int i = 0; i < db; i++)
            {
                foods.Add("food " + (i+1).ToString());
                Task.Delay(1000).Wait();
            }
        }
        public void Guest(int db)
        {
            int counter=1;
            for (int i = 0; i < db; i++)
            {
                lock (foods)
                {
                    if (foods.Count > 0)
                    {

                        Console.WriteLine(foods[0] + " removed!");
                        foods.Remove(foods[0]);
                        Task.Delay(500).Wait();
                    }
                    else
                    {
                        while (counter <= 10)
                        {
                            Console.WriteLine("No food!");
                            Task.Delay(500).Wait();
                            if (foods.Count > 0)
                            {
                                Console.WriteLine(foods[i] + " removed!");
                                foods.Remove(foods[i]);
                                Task.Delay(500).Wait();
                            }
                            counter++;
                        }
                    } 
                }
                
            }
        }
    }
}
