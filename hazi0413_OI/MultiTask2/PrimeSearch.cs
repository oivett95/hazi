﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace MultiTask2
{
    static class PrimeSearch
    {
        static List<int> numbers = Enumerable.Range(0, 10000000).ToList();
        static ConcurrentBag<int> primes = new ConcurrentBag<int>();
        static List<int> first = new List<int>();
        static List<int> second = new List<int>();

        public static bool IsPrime(this int n)
        {
            if (n <= 1) return false;
            if (n == 2) return true;
            if (n % 2 == 0) return false;
            for (int i = 3; i <= Math.Sqrt(n); i += 2)
            {
                if (n % i == 0) return false;
            }
            return true;
        }
        
        public static void Search()
        {
            for (int i = 0; i < numbers.Count; i++)
            {
                if (i < numbers.Count / 2)
                    first.Add(numbers[i]);
                else
                    second.Add(numbers[i]);
            }

            Task t1 = Task.Factory.StartNew(() =>
            {
               foreach (int n in first)
                {
                    if (n.IsPrime())
                        primes.Add(n);
                }
            });
            Task t2 = Task.Factory.StartNew(() =>
            {
                foreach (int n in second)
                {
                    if (n.IsPrime())
                        primes.Add(n);
                }
            });
            Task.WaitAll(t1, t2);
            Console.WriteLine(primes.Count);
        }
        
    }
}
