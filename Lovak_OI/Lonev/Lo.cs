﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Lonev
{
    public class Lo
    {
        public Lo(string nev, DateTime szuletesiIdo, int ar, List<String> dijak)
        {
            _nev = nev;
            _szuletesiIdo = szuletesiIdo;
            _ar = ar;
            _dijak = dijak;
        }
        public Lo()
        {

        }

        private string _nev;

        public string Nev
        {
            get { return _nev; }
            set { _nev = value; }
        }
        private DateTime _szuletesiIdo;

        public DateTime SzuletesiIdo
        {
            get { return _szuletesiIdo; }
            set { _szuletesiIdo = value; }
        }
        private int _ar;

        public int Ar
        {
            get { return _ar; }
            set { _ar = value; }
        }
        private List<string> _dijak;

        public List<string> Dijak
        {
            get { return _dijak; }
            set { _dijak = value; } //nem kell, enélkül is lehet értékeket hozzáadni
        }
    }
}
