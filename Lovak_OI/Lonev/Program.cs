﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml.Serialization;

namespace Lonev
{
    class Program
    {
        static void Main(string[] args)
        {

            const string FILENAME = "lovak.xml";
            XmlSerializer sx = new XmlSerializer(typeof(Lo []));
            if (File.Exists(FILENAME))
            {
               using (FileStream fs=new FileStream(FILENAME, FileMode.Open))
               {
                   Lo[] lovak = sx.Deserialize(fs) as Lo[];
                   foreach (Lo l in lovak)
                   {
                       Console.WriteLine("Név: "+l.Nev+"\tszületési idő: "+l.SzuletesiIdo.ToShortDateString());
                   }
               }
            }
            else
            {

                Lo[] lovak = new Lo[] { new Lo(), new Lo(), new Lo() };
                for (int i = 0; i < lovak.Length; i++)
                {
                    Console.Write("Adja meg a ló nevét: ");
                    lovak[i].Nev = Ellenoriz(Console.ReadLine());
                    lovak[i].SzuletesiIdo = DateTime.Now;
                }
                using (FileStream fs=new FileStream(FILENAME, FileMode.Create))
                {
                    sx.Serialize(fs, lovak);
                }
               
            }
            Console.ReadLine();
        }

        private static string Ellenoriz(string teljesnev)
        {
            string regex = @"^([A-ZÁÉÍÓÖÚÜŰ][a-záéíóöúüű]{2,11} ){2}([A-ZÁÉÍÓÖÚÜŰ][a-záéíóöúüű]{2,11})$";
            if (Regex.IsMatch(teljesnev, regex))
            {
                string[] reszek=teljesnev.Split(' ');
                string nev = reszek[1] +" " +reszek[2];
                Console.WriteLine("Megfelelő formátum!");
                return nev;
            }
            else 
            {
                return "Anonymus";
            }
        }
    }
}
