﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lovak_OI
{
    class Kolbasz
    {
        public Kolbasz(int ar)
        {
            _ar = ar;
        }
        private int _ar;

        public int Ar
        {
            get { return _ar; }
            set { _ar = value; }
        }
        public static explicit operator Kolbasz(Lo l)
        {
            return new Kolbasz(l.Ar / 50);
        }
        
    }
}
