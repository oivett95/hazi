﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lovak_OI
{
    class Lovarda
    {
        private static List<Lo> _lovak=new List<Lo>();
        public static void Add(Lo l)
        {
            _lovak.Add(l);
        }
        public static void CukrotOszt()
        {
            foreach (Lo l in _lovak)
            {
                l.Nyerit();
            }
        }
    }
}
