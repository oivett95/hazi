﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lovak_OI
{
    class Lorendezo
    {
        public static Lo[] Rendez(List<Lo> lovak, Comparison<Lo> comparer)
        {
            Lo[] masolat = new Lo[lovak.Count];
            lovak.CopyTo(masolat);
            Array.Sort(masolat, comparer);
            return masolat;
        }
        public static void DragaLo(List<Lo> lovak)
        {
            foreach (Lo l in lovak)
            {
                if (l.Ar>=1000)
                {
                    Console.WriteLine(l.Nev);
                }
            }
        }

       
    }
}
