﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lovak_OI
{
    class Program
    {
        static void Main(string[] args)
        {
            Fugges();
            Lista();
            Nyerit();
            Kolbasz();

            Console.ReadLine();
        }

        private static void Kolbasz()
        {
            Lo l1 = new Lo("vacsora", DateTime.Now, 500, new List<string>());
            Kolbasz k=(Kolbasz)l1;
            Console.WriteLine(k.Ar);
        }

        private static void Nyerit()
        {
            Lo l1 = new Lo();
            Lo l2 = new Poni();
            Lovarda.Add(l1);
            Lovarda.Add(l2);
            Lovarda.CukrotOszt();
        }

        private static void Lista()
        {
            List<Lo> lovak = new List<Lo>();
            Lo l1 = new Lo("ló", DateTime.Now, 200, new List<string>());
            Lo l2 = new Lo("drágaló", DateTime.Now, 1200, new List<string>());
            Lo l3 = new Lo("mégdrágábbló", DateTime.Now, 2200, new List<string>());
            lovak.Add(l1);
            lovak.Add(l2);
            lovak.Add(l3);
            Lo[] rendezett;
            rendezett = Lorendezo.Rendez(lovak, delegate(Lo a, Lo b) { return a.Nev.CompareTo(b.Nev); });
            foreach (Lo l in rendezett)
            {
                Console.WriteLine(l.Nev);
            }
            Lorendezo.DragaLo(lovak);
           
        }

        private static void Fugges()
        {
            Lo lo = new Lo();
            Fuggo fuggo = new Fuggo();
            lo.Verseny += fuggo.Fugg;
            lo.Versenyez();
        }
    }
}
